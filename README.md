# BlowMeAway #

This is the Android app part of the project that was created in the context of the hackathon [app@night](http://www.appatnight.de) 2014 for Lufthansa.

It uses multiple public and non-public APIs (500px, OpenWheatherMap, Google Maps, Lufthansa, SwissAir) as well as sensor data (GPS, compass, microphone) to provide an interactive and fun travel recommendation experience.


**Note:** *Unfortunately the app is currently not working since some APIs are not available anymore.*

### Screenshots ###

![Screenshots](img/screens_framed.png)