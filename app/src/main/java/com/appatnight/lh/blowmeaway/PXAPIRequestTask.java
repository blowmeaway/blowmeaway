package com.appatnight.lh.blowmeaway;

import android.os.AsyncTask;
import android.util.Log;
import retrofit.RestAdapter;

/**
 * Created by Anja Wahl on 08.11.2014.
 */
public class PXAPIRequestTask  extends AsyncTask<String, Void, String> {

    private static String TAG = "PXAPIRequestTask";

    private static String url = "https://api.500px.com/v1/photos/search";
    public LoadJsonReadyListener activity;
    private String term, key;


    public PXAPIRequestTask(LoadJsonReadyListener a, String term, String key) {
        Log.d(TAG, "VayantAPIRequestTask ");
        this.activity = a;
        this.term = term;
    }


    @Override
    protected String doInBackground(String... urls) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.500px.com/v1/photos")
                .build();

        PxService service = restAdapter.create(PxService.class);

        PxResponse response = service.getPictures(term);
        try {
            url = response.photos.get(0).image_url;
            Log.d("bla", url);
        } catch (Exception e) {
            Log.d("bla", "no image");
        }

        return url;

    }

    @Override
    protected void onPostExecute(String url) {
        activity.pxReadyLoaded(url);
    }

}
