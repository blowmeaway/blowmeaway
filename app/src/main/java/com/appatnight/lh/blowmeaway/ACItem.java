package com.appatnight.lh.blowmeaway;

/**
 * Created by Anja Wahl on 08.11.2014.
 */
public class ACItem {

    String code;
    String city;
    String country;

    public ACItem(String code, String city, String country){
        this.code = code;
        this.city = city;
        this.country = country;
    }

    public String getCode(){
        return code;
    }

    public String getCity(){
        return city;
    }

    public String getCountry(){
        return country;
    }


}
