package com.appatnight.lh.blowmeaway;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.*;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.google.android.gms.maps.model.LatLng;

import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.TimerTask;


public class MainActivity extends LocationActivity implements SensorEventListener {

    private static final int MIN_AMPLITUDE_BLOW = 7;

    private static final int VOLUME_POLL_FREQUENCY = 500;

    private static final int MAX_BLOW_DURATION = 3000;

    private static final int SENSOR_POLL_FREQUENCY = SensorManager.SENSOR_DELAY_NORMAL;

    private static final int REQUEST_CODE_AIRPORT_FROM = 1;

    private static final int REQUEST_CODE_AIRPORT_TO = 2;

    private static final int EARTH_RADIUS_KM = 6371;

    @InjectView(R.id.text_blow_hint)
    TextView mTextBlowHint;

    @InjectView(R.id.text_blow_instruction)
    TextView mTextBlowInstruction;

    @InjectView(R.id.paper_plane)
    ImageView mPaperPlane;

    @InjectView(R.id.paper_plane_shadow)
    ImageView mPaperPlaneShadow;

    @InjectView(R.id.compass)
    ImageView mCompass;

    private SensorManager mSensorManager;

    private Handler mHandler;

    private SoundMeter mSoundMeter;

    private boolean mBlowing = false;

    private long mBlowingStart;

    private boolean mIsFlyingOut = false;

    private boolean mAnimationFinished = false;

    private boolean mDataLoaded = false;

    private float[] mGravity;

    private float[] mGeomagnetic;

    private float mCurrentDegree = 0f;

    private long mLastSensorTime = 0;

    private float smoothingFactor = 0.2f;

    protected float[] compassVals;

    private float[] compassValues = new float[5];

    private Location mLocation;

    private String mAirportCodeFrom;

    private String mAirportCodeTo;

    String[] availableAirports = new String[]{"CGN", "DRS", "FRA", "HAM", "MUC", "NUE", "RLG", "TXL", "ADB", "AGP", "AMS", "ARN", "ATH", "AYT", "BCN", "BIA", "BIO", "BLQ", "BRI", "BUD", "CAG", "CDG", "CPH", "CTA", "DBV", "DUB", "EDI", "FAO", "FCO", "FLR", "GDN", "GOA", "GVA", "HEL", "IST", "JER", "LCA", "LCY", "LED", "LHR", "LIN", "LIS", "LPA", "MAD", "MLA", "MXP", "NAP", "NCE", "OLB", "OSL", "PMI", "PMO", "PRG", "RAK", "RAK", "RIX", "SKG", "SPU", "SUF", "TRS", "TUN", "VCE", "VIE", "VRN", "ZAD", "ZRH", "AUH", "BKK", "BOS", "CAL", "CPT", "DFW", "DXB", "EWR", "GIG", "HKG", "IAH", "JFK", "LAS", "LAX", "MCO", "MCT", "MEX", "MIA", "ORD", "PEK", "PVG", "SFO", "SIN", "TLV", "TYO", "YUL", "YVR", "YYZ"};

    private Runnable mPollVolumeRunnable = new TimerTask() {
        @Override
        public void run() {
            double amplitude = mSoundMeter.getAmplitudeEMA();
            //Log.d("BLOW", "ampl =" + amplitude);
            if (amplitude > MIN_AMPLITUDE_BLOW) {
                if (!mBlowing) {
                    mBlowing = true;
                    mBlowingStart = new Date().getTime();
                    mHandler.postDelayed(this, VOLUME_POLL_FREQUENCY);
                    flyTextOut();
                    flyPaperPlane();
                } else {
                    long blowDuration = new Date().getTime() - mBlowingStart;
                    if (blowDuration > MAX_BLOW_DURATION) {
                        onBlowFinished(blowDuration);
                    } else {
                        mHandler.postDelayed(this, VOLUME_POLL_FREQUENCY);
                    }
                }

            } else {
                if (mBlowing) {
                    long blowDuration = new Date().getTime() - mBlowingStart;
                    onBlowFinished(blowDuration);
                } else {
                    mHandler.postDelayed(this, VOLUME_POLL_FREQUENCY);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.inject(this);

        //bounceText();

        mSoundMeter = new SoundMeter();
        mHandler = new Handler();
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSoundMeter.start();
        mHandler.postDelayed(mPollVolumeRunnable, VOLUME_POLL_FREQUENCY);

        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SENSOR_POLL_FREQUENCY);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SENSOR_POLL_FREQUENCY);
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(this);

        mHandler.removeCallbacks(mPollVolumeRunnable);
        mSoundMeter.stop();
        super.onPause();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (mIsFlyingOut) {
            return;
        }

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            mGravity = event.values;
        }
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            mGeomagnetic =  event.values;
        }

        long currentTime = new Date().getTime();
        long lastSensorDelta = currentTime - mLastSensorTime;

        if (mGravity != null && mGeomagnetic != null && lastSensorDelta >20) {
            mLastSensorTime = currentTime;

            float R[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, null, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                float azimuth = (float) Math.toDegrees(setmValues(orientation));

                updateCompass(azimuth);
            }
        }
    }

    protected float[] lowPass( float[] input, float[] output ) {
        if ( output == null ) return input;

        for ( int i=0; i<input.length; i++ ) {
            output[i] = output[i] + smoothingFactor * (input[i] - output[i]);
        }
        return output;
    }


    public float setmValues(float[] mValues) {

        compassValues[0]= compassValues[1];

        compassValues[1]= compassValues[2];

        compassValues[2]= compassValues[3];

        compassValues[3]= compassValues[4];

        compassValues[4]= mValues[0];

        return (compassValues[0] +compassValues[1] +compassValues[2] +compassValues[3] +compassValues[4])/5;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void updateCompass(float degree) {
        if (Math.abs(mCurrentDegree + degree) == 0) {
            return;
        }

        RotateAnimation ra = new RotateAnimation(
                mCurrentDegree,
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);

        ra.setDuration(20);
        //ra.setInterpolator(new AccelerateDecelerateInterpolator());
        ra.setFillAfter(true);

        mCompass.startAnimation(ra);
        mCurrentDegree = -degree;
    }

    private void bounceText() {
        Animation bounceAnimation = AnimationUtils.loadAnimation(this, R.anim.text_bounce);
        bounceAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!mIsFlyingOut) {
                    bounceText();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        mTextBlowHint.startAnimation(bounceAnimation);
    }

    private void onBlowFinished(long duration) {
        Log.d("bla", "blow duration: " + duration);
        int randomizedDuration = (int) duration + (int) ((Math.random() - 0.5) * 500);
        findTargetAirport(randomizedDuration);
    }

    private void flyTextOut() {
        mIsFlyingOut = true;
        Animation flyOutAnimation = AnimationUtils.loadAnimation(this, R.anim.text_fly_out);
        mTextBlowInstruction.startAnimation(flyOutAnimation);
        mTextBlowHint.clearAnimation();
        mTextBlowHint.startAnimation(flyOutAnimation);

        mCompass.animate()
                .alpha(0)
                .rotation(360)
                .setInterpolator(new AccelerateInterpolator())
                .setDuration(1000)
                .start();
    }

    private void flyPaperPlane() {
        mPaperPlane.animate()
                .alpha(0f)
                .scaleX(0.3f)
                .scaleY(0.3f)
                .y(-mPaperPlane.getHeight())
                .setDuration(3000)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();

        mPaperPlaneShadow.animate()
                .alpha(-0.5f)
                .scaleX(0.2f)
                .scaleY(0.2f)
                .y(-mPaperPlaneShadow.getHeight())
                .setDuration(3500)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        mAnimationFinished = true;
                        if (mDataLoaded) {
                            startDetails();
                        }
                    }
                })
                .start();
    }

    @Override
    public void onLocationClientConnected() {
        mLocation = getLocationClient().getLastLocation();
        new NearestAirportRequestTask(this, REQUEST_CODE_AIRPORT_FROM)
                .execute(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()));
    }

    private void findTargetAirport(int distance) {
        float bearing = mCurrentDegree;
        int targetDistance = distance * 1000;
        Log.d("bla", "target distance = " + targetDistance / 1000);
        LatLng targetPoint = moveLocation(mLocation, bearing, targetDistance);
        new NearestAirportRequestTask(this, REQUEST_CODE_AIRPORT_TO).execute(targetPoint);
    }

    public LatLng moveLocation(Location start, double bearing, double distance) {
        double bR = Math.toRadians(bearing);
        double lat1R = Math.toRadians(start.getLatitude());
        double lon1R = Math.toRadians(start.getLongitude());
        double dR = distance / (EARTH_RADIUS_KM * 1000);

        double a = Math.sin(dR) * Math.cos(lat1R);
        double lat2 = Math.asin(Math.sin(lat1R) * Math.cos(dR) + a * Math.cos(bR));
        double lon2 = lon1R
                + Math.atan2(Math.sin(bR) * a, Math.cos(dR) - Math.sin(lat1R) * Math.sin(lat2));
        return new LatLng(Math.toDegrees(lat2), Math.toDegrees(lon2));
    }

    public void onNearestAirportFound(String airportCode, int requestCode) {
        if (airportCode == null) {
            Toast.makeText(this, "Error: could not get airport code", Toast.LENGTH_SHORT).show();
        }

        if (requestCode == REQUEST_CODE_AIRPORT_FROM) {
            mAirportCodeFrom = airportCode;
        } else {
            mAirportCodeTo = airportCode;
            if (!Arrays.asList(availableAirports).contains(mAirportCodeTo)) {
                Toast.makeText(this, "Airport " + mAirportCodeTo + " not available! Choose random one", Toast.LENGTH_SHORT).show();
                mAirportCodeTo = availableAirports[new Random().nextInt(availableAirports.length) -1];
            }
        }

        mDataLoaded = true;

        if (mAirportCodeTo != null && mAirportCodeFrom != null && mAnimationFinished) {
            startDetails();
        }

    }

    private void startDetails() {
        Log.d("bla", "startDetail, from " + mAirportCodeFrom + " to " + mAirportCodeTo);
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra("fromDestination", mAirportCodeFrom);
        intent.putExtra("toDestination", mAirportCodeTo);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

}
