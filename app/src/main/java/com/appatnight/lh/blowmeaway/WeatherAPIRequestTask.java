package com.appatnight.lh.blowmeaway;

import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

import retrofit.RestAdapter;

public class WeatherAPIRequestTask extends AsyncTask<String, Integer, List<WeatherDay>> {


    private static String TAG = "WeatherAPIRequestTask";

    private static String url = "http://api.openweathermap.org/data/2.5/forecast";
    public LoadJsonReadyListener activity;
    private String term, key;


    public WeatherAPIRequestTask(LoadJsonReadyListener a, String term, String key) {
        Log.d(TAG, "WeatherAPIRequestTask");
        this.activity = a;
        this.term = term;
    }

    @Override
    protected List<WeatherDay> doInBackground(String... urls) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://api.openweathermap.org/data/2.5/forecast")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        WeatherService service = restAdapter.create(WeatherService.class);

        WeatherResponse response = service.getWeather(term);
        List<WeatherDay> days = response.list;
        return days;
    }

    @Override
    protected void onPostExecute(List<WeatherDay> days) {
        activity.weatherReadyLoaded(days);
    }

}
