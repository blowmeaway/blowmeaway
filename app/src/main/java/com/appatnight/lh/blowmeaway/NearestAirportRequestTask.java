package com.appatnight.lh.blowmeaway;

import android.os.AsyncTask;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class NearestAirportRequestTask extends AsyncTask<LatLng, Integer, String> {

    private static final String REQUEST_BODY = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
    "<s:Body xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
    "<RequestNearestAirportInformation xmlns=\"http://www.swiss.com/\">" +
    "<ota_ScreenTextRQ MessageFunction=\"%s,%s\" PrimaryLangID=\"de\" Version=\"1\" xmlns=\"http://www.opentravel.org/OTA/2003/05\">" +
    "<POS>" +
    "<Source>" +
    "<RequestorID ID=\"195004ec-da2a-4cba-8db0-f0ad20094802\" MessagePassword=\"61022420B9C8FA46999094D06379D83B\"/>" +
    "</Source>" +
    "</POS>" +
    "</ota_ScreenTextRQ>" +
    "</RequestNearestAirportInformation>" +
    "</s:Body>" +
    "</s:Envelope>";

    private MainActivity mActivity;

    private int mCode;

    public NearestAirportRequestTask(MainActivity activity, int requestCode) {
        mActivity = activity;
        mCode = requestCode;
    }

    @Override
    protected String doInBackground(LatLng... params) {
        double lat = params[0].latitude;
        double lng = params[0].longitude;

        StringEntity se;
        try {
            se = new StringEntity(getBody(lat, lng), HTTP.UTF_8);
            se.setContentType("text/xml");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }

        HttpPost httppost = new HttpPost("http://www.swiss.com//WebServices/SwissService.asmx");
        httppost.addHeader("SOAPAction", "http://www.swiss.com/RequestNearestAirportInformation");
        httppost.setEntity(se);

        HttpClient httpclient = new DefaultHttpClient();
        BasicHttpResponse httpResponse;
        try {
            httpResponse = (BasicHttpResponse) httpclient.execute(httppost);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        String response;
        try {
            response = IOUtils.toString(httpResponse.getEntity().getContent(), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        Log.d("bla", response);
        return getAirportCode(response);
    }

    @Override
    protected void onPostExecute(String s) {
        Log.d("bla", s);
        mActivity.onNearestAirportFound(s, mCode);
    }

    private String getBody(double lat, double lng) {
        return String.format(REQUEST_BODY, lng, lat);
    }

    private String getAirportCode(String xml) {
        String token = "<TextData>Code#";
        int start = xml.indexOf(token);
        if (start == -1) {
            return null;
        }

        return xml.substring(start + token.length(), start + token.length() + 3);
    }

}
