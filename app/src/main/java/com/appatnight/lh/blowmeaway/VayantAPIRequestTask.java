package com.appatnight.lh.blowmeaway;

/**
 * Created by Anja Wahl on 08.11.2014.
 */

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class VayantAPIRequestTask extends AsyncTask<String, Void, String> {
    private static String TAG = "VayantAPIRequestTask";

    private static String url = "http://lh-fs-json.production.vayant.com";
    public LoadJsonReadyListener activity;

    private String from, to, depart_from;
    int stay;


    public VayantAPIRequestTask(LoadJsonReadyListener a, String from, String to, String depart_from, int stay) {
        Log.d(TAG, "VayantAPIRequestTask " + depart_from + " " + stay );
        this.activity = a;
        this.from = from;
        this.to = to;
        this.depart_from = depart_from;
        this.stay = stay;
    }


    @Override
    protected String doInBackground(String... urls) {
        Log.d(TAG, "doInBackground URL: " + url);
        try {
            return parseToFlightItem(loadJsonFromNetwork(url));
        } catch (IOException e) {
            Log.d(TAG, "APIRequestTask: connection error");
            return null;
        } catch (XmlPullParserException e) {
            Log.d(TAG, "APIRequestTask: json error");
            return null;
        }

    }

    @Override
    protected void onPostExecute(String price) {
        activity.jsonReadyLoaded(price);
    }

    private JSONArray loadJsonFromNetwork(String urlString) throws XmlPullParserException, IOException {
        InputStream stream = null;
        String json = "";
        JSONArray jArr = null;
        Log.d(TAG, "APIRequestTask::loadJsonFromNetwork()");
        try {
            json = downloadUrl(urlString);
            try {
// Dealing with JSONArrays and JSONObjects
                if (json.startsWith("{")) {
                    String json2 = "[" + json + "]";
                    jArr = new JSONArray(json2);
                } else {
                    jArr = new JSONArray(json);
                }
            } catch (JSONException e) {
                Log.d("JSON Parser", "Error parsing datas " + e.toString());
            }
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
        return jArr;
    }

    // Given a string representation of a URL, sets up a connection and gets
// an input stream.
    private String downloadUrl(String urlString) throws IOException {
        Log.d(TAG, "APIRequestTask::downloadUrl() urlString " + urlString);
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);

        conn.setRequestMethod("POST");

        conn.setDoInput(true);
        conn.setDoOutput(true);


        String postData = "{\"User\": " + "\"LufthansaTest\", " +
                " \"Pass\": \"8b35317451999984abf8bf38b5863341da2b2e97\"," +
                "\"Environment\": \"lh-vzg\"," +
                "\"Origin\": \"" + from + "\"," +
                "\"Destination\": \"" + to + "\"," +
                "\"DepartureFrom\":\"" + depart_from + "\"," +
                "\"LengthOfStay\":" + stay + "," +
                "\"Passengers\": [{\"Type\": \"ADT\", \"Count\": 1}]," +
                "\"GroupBy\": \"CityPair\"," +
                "\"Response\":\"light\"}";


        String paramString = postData.toString();
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Content-Length", "" + Integer.toString(paramString.getBytes().length));
        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
        wr.writeBytes(paramString);
        wr.flush();
        wr.close();

        Log.d(TAG, "APIRequestTask::downloadUrl() conn.connect()" + conn);
        InputStream s;
        try

        {
            int status = conn.getResponseCode();
            Log.d(TAG, "APIRequestTask::downloadUrl() status" + status);
            s = conn.getInputStream();
        } catch (
                Exception e
                )

        {
            e.printStackTrace();
            Log.d(TAG, "APIRequestTask::downloadUrl() error");
            s = conn.getErrorStream();
        }

        BufferedReader reader;
        StringBuilder sb = new StringBuilder();
        try

        {
            reader = new BufferedReader(new InputStreamReader(s, "UTF-8"), 8);
            String line = null;
            while ((line = reader.readLine()) != null) {
                Log.d(TAG, "APIRequestTask: BufferedReader: " + line);
                sb.append(line + "\n");
            }
        } catch (
                Exception e1
                )

        {
            e1.printStackTrace();
        }

        return sb.toString();
    }

    private String parseToFlightItem(JSONArray json) {
        String price = "";
        try {
            for (int j = 0; j < json.length(); j++) {
                JSONObject result = json.getJSONObject(j);
                try {
                    JSONObject cityPairs = result.getJSONObject("CityPairs");
                    try {
                        JSONObject cityPairs2 = cityPairs.getJSONObject(from + to);
                        price = "ab " + cityPairs2.getString("min") + "€";

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    Log.d(TAG, "APIRequestTask: parseToFlightItem: No Result");
                    price = "No flights available";
                    e.printStackTrace();
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return price;

    }

}


