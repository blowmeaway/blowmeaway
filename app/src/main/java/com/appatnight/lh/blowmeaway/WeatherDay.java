package com.appatnight.lh.blowmeaway;

import java.util.List;

/**
 * Created by Max on 08.11.2014.
 */
public class WeatherDay {

    String image_url;

    long dt;
    List<WeatherDetail> weather;
    WeatherTemp temp;

    public static class WeatherTemp{
        float day;
        float max;
        float min;
    };

    public static class WeatherDetail{
        String main;
        String description;
        String icon;
    };
}
