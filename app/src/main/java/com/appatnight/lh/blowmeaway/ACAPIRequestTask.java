package com.appatnight.lh.blowmeaway;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class ACAPIRequestTask extends AsyncTask<String, Void, ACItem> {
    private static String TAG = "ACAPIRequestTask";

    private static String url = "https://airport.api.aero/airport?user_key=622eb86cf33c2fa9a3d6717417b37bb1";
    public LoadJsonReadyListener activity;

    private String code;


    public ACAPIRequestTask(LoadJsonReadyListener a, String code) {
        Log.d(TAG, "ACAPIRequestTask ");
        this.activity = a;
        this.code = code;
    }


    @Override
    protected ACItem doInBackground(String... urls) {
        Log.d(TAG, "doInBackground URL: " + url);
        try {
            return parseToACItem(loadJsonFromNetwork(url));
        } catch (IOException e) {
            Log.d(TAG, "APIRequestTask: connection error");
            return null;
        } catch (XmlPullParserException e) {
            Log.d(TAG, "APIRequestTask: json error");
            return null;
        }

    }

    @Override
    protected void onPostExecute(ACItem aci) {
        activity.acReadyLoaded(aci);
    }

    private String loadJsonFromNetwork(String urlString) throws XmlPullParserException, IOException {
        InputStream stream = null;
        String json = "";
        JSONArray jArr = null;
        Log.d(TAG, "APIRequestTask::loadJsonFromNetwork()");
        try {
            json = downloadUrl(urlString);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
        return json;
    }

    // Given a string representation of a URL, sets up a connection and gets
// an input stream.
    private String downloadUrl(String urlString) throws IOException {

        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);

        conn.setRequestMethod("GET");


        conn.setDoInput(true);
        conn.setDoOutput(true);

        Log.d("codivo", "APIRequestTask::downloadUrl() conn.connect()" + conn);
        InputStream s;
        try {
            int status = conn.getResponseCode();
            Log.d("codivo", "APIRequestTask::downloadUrl() status" + status);
            s = conn.getInputStream();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("codivo", "APIRequestTask::downloadUrl() error");
            s = conn.getErrorStream();
        }
        BufferedReader reader;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(s, "UTF-8"), 8);
            String line = null;
            while ((line = reader.readLine()) != null) {
                Log.d("codivo", "APIRequestTask: BufferedReader: " + line);
                sb.append(line + "\n");
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return sb.toString();
    }

    private ACItem parseToACItem(String s) {
        s.replace("callback(", "").replace("(", "");

        JSONObject json = null;
        try {
            json = new JSONObject(s);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        ACItem aci = null;
        try {
            JSONArray result = json.getJSONArray("airports");
            for (int i = 0; i < result.length(); i++) {
                JSONObject airport = result.getJSONObject(i);
                String code = "";
                String city = "";
                String country = "";


                try {
                    code = airport.getString("code");
                    city = airport.getString("city");
                    country = airport.getString("country");

                    aci = new ACItem(code, city, country);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        return aci;

    }

}
