package com.appatnight.lh.blowmeaway;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class DetailActivity extends ActionBarActivity implements LoadJsonReadyListener {
    private static String TAG = "DetailActivity";

    private static int DEFAULT_LENGTH_OF_STAY = 3;
    private static int DEFAULT_START_DATE_FROM_NOW = 7;

    private String fromDestination;
    private String toDestination;

    TextView weather_tmp[] = new TextView[3];
    ImageView weather_img[] = new ImageView[3];
    TextView weather_dow[] = new TextView[3];

    TextView detail_title, detail_country, detail_date_start, detail_date_return, detail_price, code_from, code_to;
    private int currentDateTypeForPicker = 1;
    Calendar cal_start, cal_return;
    DetailActivity c;
    SimpleDateFormat sdf;
    Map<String, String> mapCity, mapCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Log.d(TAG, "DetailActivity::onCreate");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            fromDestination = extras.getString("fromDestination");
            toDestination = extras.getString("toDestination");
        }

        c = this;

        loadCSV();

        String myFormat = "yyyy-MM-dd"; //In which you need put here
        sdf = new SimpleDateFormat(myFormat, Locale.US);

        initCal();

        Log.d(TAG, "DetailActivity::onCreate sdf.format(cal_start.getTime())" + sdf.format(cal_start.getTime()));

        new VayantAPIRequestTask(this, fromDestination, toDestination, sdf.format(cal_start.getTime()), computeStay()).execute();

        new PXAPIRequestTask(this, mapCity.get(toDestination), getString(R.string.px_consumer_key)).execute();

        new WeatherAPIRequestTask(this, mapCity.get(toDestination), getResources().getString(R.string.weather_key)).execute();

        initLayout();


    }

    private void initCal() {
        cal_start = Calendar.getInstance();
        cal_start.add(Calendar.DAY_OF_YEAR, DEFAULT_START_DATE_FROM_NOW);
        cal_return = Calendar.getInstance();
        cal_return.add(Calendar.DAY_OF_YEAR, DEFAULT_START_DATE_FROM_NOW + DEFAULT_LENGTH_OF_STAY);
    }

    private int computeStay() {
        long diff = cal_return.getTimeInMillis() - cal_start.getTimeInMillis();
        return (int) (diff / (24 * 60 * 60 * 1000));
    }

    private void initLayout() {
        detail_title = (TextView) findViewById(R.id.detail_title);
        detail_title.setText(mapCity.get(toDestination));
        detail_country = (TextView) findViewById(R.id.detail_country);
        detail_country.setText(mapCountry.get(toDestination));
        code_from = (TextView) findViewById(R.id.text_code_from);
        code_from.setText(fromDestination);
        code_to = (TextView) findViewById(R.id.text_code_to);
        code_to.setText(toDestination);

        detail_price = (TextView) findViewById(R.id.detail_price);


        detail_date_start = (TextView) findViewById(R.id.detail_date_start);
        detail_date_start.setText(cal_start.get(Calendar.DAY_OF_MONTH) + "." + (cal_start.get(Calendar.MONTH) + 1) + "." + cal_start.get(Calendar.YEAR));


        detail_date_return = (TextView) findViewById(R.id.detail_date_return);
        detail_date_return.setText(cal_return.get(Calendar.DAY_OF_MONTH) + "." + (cal_return.get(Calendar.MONTH) + 1) + "." + cal_return.get(Calendar.YEAR));

        Picasso.with(this)
                .load("https://maps.googleapis.com/maps/api/staticmap?parameters&size=600x300&maptype=roadmap&sensor=false&zoom=4&scale=2&center="
                        + mapCity.get(toDestination)
                        + "&markers=size:medium%7Ccolor:0xffc500%7Clabel:%7C"
                        + mapCity.get(toDestination))
                .placeholder(R.drawable.map)
                .into((ImageView) findViewById(R.id.map_view));


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                Log.d(TAG, "DetailActivity::onDateSet");
                if (currentDateTypeForPicker == 1) {
                    cal_start.set(year, monthOfYear, dayOfMonth);
                    detail_date_start.setText(cal_start.get(Calendar.DAY_OF_MONTH) + "." + (cal_start.get(Calendar.MONTH) + 1) + "." + cal_start.get(Calendar.YEAR));


                } else if (currentDateTypeForPicker == 2) {
                    cal_return.set(year, monthOfYear, dayOfMonth);
                    detail_date_return.setText(cal_return.get(Calendar.DAY_OF_MONTH) + "." + (cal_return.get(Calendar.MONTH) + 1) + "." + cal_return.get(Calendar.YEAR));

                }

                new VayantAPIRequestTask(c, fromDestination, toDestination, sdf.format(cal_start.getTime()), computeStay()).execute();

            }

        };

        detail_date_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "DetailActivity::change detail_date_start");
                currentDateTypeForPicker = 1;
                new DatePickerDialog(c, date, cal_start
                        .get(Calendar.YEAR), cal_start.get(Calendar.MONTH),
                        cal_start.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        detail_date_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "DetailActivity::change detail_date_return");
                currentDateTypeForPicker = 2;
                new DatePickerDialog(c, date, cal_return
                        .get(Calendar.YEAR), cal_return.get(Calendar.MONTH),
                        cal_return.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        ImageView circle = (ImageView) findViewById(R.id.circle_button);
        circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(getLhUrl()));
                startActivity(i);
            }
        });
    }

    @Override
    public void jsonReadyLoaded(String price) {
        Log.d(TAG, "DetailActivity::jsonReadyLoaded" + price);
        detail_price.setText(price);
    }

    @Override
    public void pxReadyLoaded(String url) {
        Log.d(TAG, "DetailActivity::pxReadyLoaded" + url);
        if (url != null) {
            Picasso.with(this)
                    .load(url)
                    .placeholder(R.drawable.clouds)
                    .into((ImageView) findViewById(R.id.detail_image));
        }
    }


    @Override
    public void acReadyLoaded(ACItem aci) {
        Log.d(TAG, "DetailActivity::acReadyLoaded" + aci);
        if (aci.code.equals(toDestination)) {
            detail_title.setText(aci.city);
        }
    }

    private void loadCSV() {
        //String csvFile = "/Users/mkyong/Downloads/GeoIPCountryWhois.csv";

        InputStream is = null;
        try {
            is = getResources().getAssets().open("AirportList.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";

        try {

            mapCity = new HashMap<String, String>();
            mapCountry = new HashMap<String, String>();

            br = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] country = line.split(cvsSplitBy);

                mapCity.put(country[1], country[3]);
                mapCountry.put(country[1], country[4]);

            }

            //loop map
          /*  for (Map.Entry<String, String> entry : maps.entrySet()) {

                System.out.println("Country [code= " + entry.getKey() + " , name="
                        + entry.getValue() + "]");

            }*/

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        //System.out.println("Done");
    }

    public void weatherReadyLoaded(List<WeatherDay> days) {
        //Log.d(TAG, "DetailActivity::weatherLoaded" + days);

        //Log.d(TAG, "" + days.size());
        //Log.d(TAG, "" + days.get(0).weather.get(0).main);
        //Log.d(TAG, days.get(0).detail.main);


        //cal_start.set(Calendar.DATE);
        long startInMillis = cal_start.getTimeInMillis();

        int weatherPos = 0;
        for (int i = 0; i < days.size(); i++) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis((long)days.get(i).dt * 1000);

            //weather_1_dow weather_1_img weather_1_tmp
            //Log.d(TAG, "Weather Pos " + weatherPos);

            if (cal_start.get(Calendar.DAY_OF_YEAR)+weatherPos == (cal.get(Calendar.DAY_OF_YEAR))) {
                Log.d(TAG, "DetailActivity::weatherLoaded on Day" + cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US));

                if(weatherPos == 0 ){
                    weather_tmp[0] = (TextView) findViewById(R.id.weather_1_tmp);
                    weather_tmp[0].setText((int)days.get(i).temp.day + "°");

                    weather_dow[0] = (TextView) findViewById(R.id.weather_1_dow);
                    weather_dow[0].setText(cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US));

                    weather_img[0] = (ImageView) findViewById(R.id.weather_1_img);
                    int resourceId = this.getResources().getIdentifier( "w"+days.get(i).weather.get(0).icon, "drawable", this.getBaseContext().getPackageName());


                    if (resourceId == 0) {
                        weather_img[0].setImageResource(R.drawable.w00);
                    } else {
                        weather_img[0].setImageResource(resourceId);
                    }

                    weatherPos++;
                } else if(weatherPos == 1){
                    weather_tmp[1] = (TextView) findViewById(R.id.weather_2_tmp);
                    weather_tmp[1].setText((int)days.get(i).temp.day + "°");
                    weather_dow[1] = (TextView) findViewById(R.id.weather_2_dow);
                    weather_dow[1].setText(cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US));
                    weather_img[1] = (ImageView) findViewById(R.id.weather_2_img);
                    int resourceId = this.getResources().getIdentifier( "w"+days.get(i).weather.get(0).icon, "drawable", this.getBaseContext().getPackageName());
                    if (resourceId == 0) {
                        weather_img[1].setImageResource(R.drawable.w00);
                    } else {
                        weather_img[1].setImageResource(resourceId);
                    }

                    weatherPos++;
                } else if(weatherPos == 2){
                    weather_tmp[2] = (TextView) findViewById(R.id.weather_3_tmp);
                    weather_tmp[2].setText((int)days.get(i).temp.day + "°");
                    weather_dow[2] = (TextView) findViewById(R.id.weather_3_dow);
                    weather_dow[2].setText(cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US));
                    weather_img[2] = (ImageView) findViewById(R.id.weather_3_img);
                    int resourceId = this.getResources().getIdentifier( "w"+days.get(i).weather.get(0).icon, "drawable", this.getBaseContext().getPackageName());
                    if (resourceId == 0) {
                       weather_img[2].setImageResource(R.drawable.w00);
                    } else {
                        weather_img[2].setImageResource(resourceId);
                    }

                }


            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private String getLhUrl() {
        String format = "http://book.lufthansa.com/lh/dyn/air-lh/revenue/viewFlights?WDS_LIST_CD_CODE_1=ZIN812400&WDS_LIST_CD_CODE_3=ZE782675&PORTAL_SESSION=XHAe0fkRVlawQfz4gyqYxcm&B_LOCATION_1=%s&WDS_SUPER_FLEX_FARE=FALSE&WDS_LIST_CD_CODE_2=ZDL208200&IP_COUNTRY=DE&WDS_WR_DCSID=dcsi8dl8n100000kbqfxzervo_7i6g&PORTAL=LH&E_LOCATION_1=%s&SERVICE_ID=0&SO_SITE_COUNTRY_OF_RESIDENCE=DE&NB_INF=0&SITE=LUFTLUFT&COUNTRY_SITE=DE&WDS_WR_BFT=K&TRIP_TYPE=R&FORCE_OVERRIDE=TRUE&SECURE=FALSE&WDS_WR_SCENARIO=S1&WDS_WR_AREA=%s&B_DATE_1=%s0000&B_DATE_2=%s0000&NB_CHD=0&LANGUAGE=DE&NB_ADT=1&POS=DE&WDS_WR_DCSEXT_SCENARIO=1&CABIN=E&WDS_WR_CHANNEL=LHCOM&LIST_CD_CODE=ZE782675&DEVICE_TYPE=MOBILE";

        String[] start = detail_date_start.getText().toString().split("\\.");
        String[] end = detail_date_return.getText().toString().split("\\.");

        String dateStringStart = start[2] + start[1] + start[0];
        String dateStringEnd = end[2] + end[1] + end[0];

        return String.format(format, fromDestination, toDestination, toDestination, dateStringStart, dateStringEnd);
    }

}
