package com.appatnight.lh.blowmeaway;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Max on 08.11.2014.
 */
public interface WeatherService {

     @GET("/daily?key=cbec4139e5993b5cc37380c63854c01a&cnt=16&mode=json&units=metric")
     WeatherResponse getWeather(@Query("q") String city);

}
