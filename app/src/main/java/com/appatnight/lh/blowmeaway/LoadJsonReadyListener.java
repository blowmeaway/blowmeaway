package com.appatnight.lh.blowmeaway;

import org.json.JSONArray;

import java.util.List;
public interface LoadJsonReadyListener {
    public void jsonReadyLoaded(final String price);
    public void pxReadyLoaded(final String url);
    public void acReadyLoaded(final ACItem aci);
    public void weatherReadyLoaded(final List<WeatherDay> days);

}
