package com.appatnight.lh.blowmeaway;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Max on 08.11.2014.
 */
public class WeatherResponse {

    @SerializedName("list")
    List<WeatherDay> list;
}
