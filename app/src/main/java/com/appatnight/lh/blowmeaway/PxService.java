package com.appatnight.lh.blowmeaway;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public interface PxService {

    @GET("/search?tag=landmark&consumer_key=P4OK1UfdH95EpiQ0Llbvt8RHNcTw25fnMwnXJEXk&image_size=4")
    PxResponse getPictures(@Query("term") String term);

}
